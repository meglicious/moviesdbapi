FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src
COPY ["MoviesDb3/MoviesDb3.csproj", "MoviesDb3/"]
RUN dotnet restore "MoviesDb3/MoviesDb3.csproj"
COPY . .
WORKDIR "/src/MoviesDb3"
RUN dotnet build "MoviesDb3.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "MoviesDb3.csproj" -c Release -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "MoviesDb3.dll"]
