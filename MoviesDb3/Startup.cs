﻿using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using System.Reflection;
using TheMoviesDB;

namespace MoviesDb3
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)


        {
            // Adding DbContext to the dependency injection container
            services.AddDbContext<MoviesDbContext>(Option => Option.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
            // Adding AutoMapper to the dependency injection container
            services.AddAutoMapper(typeof(Startup));
            

            // Adding Controllers to the dependency injection container
            services.AddControllers();

            // Adding Swagger to the dependency injection container
            services.AddSwaggerGen(c =>
            {
                // Adding OpenApiInfo to the SwaggerDoc
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "MoviesDb API",
                    Description = "Assignment 3 Movies Database ASP.NET Core Web API",
                    TermsOfService = new Uri("https://example.com/terms"),
                    Contact = new OpenApiContact
                    {
                        Name = "Meg Stefouli",
                        Url = new Uri("https://gitlab.com/meglicious"),
                    },
                    License = new OpenApiLicense
                    {
                        Name = "Meg Stefouli",
                        Url = new Uri("https://gitlab.com/meglicious"),
                    }
                });
                //Set the comments path for the Swagger JSON and UI
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);

            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            // If the environment is set to Development, show the developer exception page
            // and setup Swagger for API documentation
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "MoviesDbAPI v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }

}

