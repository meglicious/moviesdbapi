﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheMoviesDB.Model
{
    public class Franchise
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(20)] public string Name { get; set; }
        [Required]
        [MaxLength(200)] public string Description { get; set; }

        public ICollection<Movie> Movies { get; set; }
    }
}
