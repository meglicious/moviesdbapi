﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations; //for data annotation = client side validation
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheMoviesDB.Model
{
    public class Character //Entity - to create a table
    {

        public int Id { get; set; }
        [Required]
        [MaxLength(35)] public string FullName { get; set; }
        [MaxLength(15)] public string? Alias { get; set; }

        [MaxLength(7)] public string Gender { get; set; }

        public string Picture { get; set; }

        //Set the foreign key, relationships

        //Navigation property set here
        public ICollection<Movie> Movies { get; set; }

    }
}
