﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheMoviesDB.Model
{
    public class Movie
    {
        //properties for the database table fields = Columns
        public int Id { get; set; }
        [Required]
        [MaxLength(40)] public string MovieTitle { get; set; }
        [MaxLength(15)] public string Genre { get; set; }
        [Required]
        [MaxLength(4)] public int ReleaseYear { get; set; }
        [MaxLength(30)] public string Director { get; set; }
        public string Picture { get; set; }
        public string Trailer { get; set; }


        public ICollection<Character> Characters { get; set; } //Navigation collection property
        public int? FranchiseId { get; set; }
        public Franchise Franchise { get; set; }
    }
}
