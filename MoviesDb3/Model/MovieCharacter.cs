﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheMoviesDB.Model
{
    public class MovieCharacter
    {
        //Set the properties
        [Required]
        public int ChacacterId { get; set; }
        public Character Character { get; set; }
        [Required]
        public int MovieId { get; set; }
        public Movie Movie { get; set; }
    }
}
