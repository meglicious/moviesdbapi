﻿namespace MoviesDb3.DataTObj.DTOCharacter
{
    public class DTOReadCharacter
    {
        public string FullName { get; set; }
        public string Alias { get; set; }
        public string Gender { get; set; }
        public string PictureURL { get; set; }

        public List<string> Movies { get; set; }

    }
}
