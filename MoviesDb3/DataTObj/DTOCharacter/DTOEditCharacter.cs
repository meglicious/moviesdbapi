﻿namespace MoviesDb3.DataTObj.DTOCharacter
{
    public class DTOEditCharacter
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Alias { get; set; }
        public string Gender { get; set; }
        public string PictureURL { get; set; }

    }
}
