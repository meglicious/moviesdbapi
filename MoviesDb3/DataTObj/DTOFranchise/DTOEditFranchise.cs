﻿namespace MoviesDb3.DataTObj.DTOFranchise
{
    public class DTOEditFranchise
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

    }
}
