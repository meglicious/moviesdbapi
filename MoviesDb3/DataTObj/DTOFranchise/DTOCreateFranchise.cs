﻿namespace MoviesDb3.DataTObj.DTOFranchise
{
    public class DTOCreateFranchise
    {
        public string Name { get; set; }
        public string Description { get; set; }

    }
}
