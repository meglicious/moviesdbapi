﻿namespace MoviesDb3.DataTObj.DTOFranchise
{
    public class DTOReadFranchise
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<string> Movies { get; set; }
    }
}
