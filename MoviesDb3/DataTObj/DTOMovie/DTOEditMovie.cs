﻿namespace MoviesDb3.DataTObj.CTOMovie
{
    public class DTOEditMovie
    {
        public int Id { get; set; }
        public string MovieTitle { get; set; }
        public int ReleaseYear { get; set; }
        public string Director { get; set; }
        public string PictureURL { get; set; }
        public string TrailerURL { get; set; }

    }
}
