﻿namespace MoviesDb3.DataTObj
{
    public class DTOReadMovie
    {
        public int Id { get; set; }
        public string MovieTitle { get; set; }

        public int ReleaseYear { get; set; }
        public string Director { get; set; }
        public string Picture { get; set; }
        public string Trailer { get; set; }

    }
}
