﻿using System.ComponentModel.DataAnnotations;
using TheMoviesDB.Model;

namespace MoviesDb3.DataTObj.CTOMovie
{
    public class DTOCreateMovie
    {

        //set properties
        public string MovieTitle { get; set; }

        public int ReleaseYear { get; set; }
        public string Director { get; set; }
        public string Picture { get; set; }
        public string Trailer { get; set; }



        public int? FranchiseId { get; set; }

    }
}
