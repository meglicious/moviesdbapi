﻿
using AutoMapper;
using MoviesDb3.DataTObj.CTOMovie;
using MoviesDb3.DataTObj.DTOFranchise;
using TheMoviesDB.Model;

namespace MoviesDb3.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile() 
        {
            // Mapping  Franchise to DTOReadFranchise
            CreateMap<Franchise, DTOReadFranchise>().ForMember(adto => adto.Movies, opt => opt
                    .MapFrom(a => a.Movies.Select(c => c.MovieTitle).ToArray()));

            // Mapping DTOEditFranchise to Franchise
            CreateMap<DTOEditFranchise, Franchise>();

            // Mapping DTOCreateFranchise to Franchise
            CreateMap<DTOCreateFranchise, Franchise>();
        }   
        
    }
}
