﻿using AutoMapper;
using MoviesDb3.DataTObj;
using MoviesDb3.DataTObj.CTOMovie;
using TheMoviesDB.Model;

namespace MoviesDb3.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            // Mapping  Movie to DTOReadMovie
            CreateMap<Movie, DTOReadMovie>().ForMember(bdto => bdto.Id, opt => opt
                    .MapFrom(b => b.Characters.Select(c => c.FullName).ToArray()));

            // Mapping DTOEditMovie to Movie
            CreateMap<DTOEditMovie, Movie>();

            // Mapping DTOCreateMovie to Movie
            CreateMap<DTOCreateMovie, Movie>();
        }
    }
}
