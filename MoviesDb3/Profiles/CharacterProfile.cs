﻿using AutoMapper;
using MoviesDb3.DataTObj.CTOMovie;
using MoviesDb3.DataTObj.DTOCharacter;
using MoviesDb3.DataTObj.DTOFranchise;
using TheMoviesDB.Model;

namespace MoviesDb3.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            // Mapping  Character to DTOReadCharacter
            CreateMap<Character, DTOReadCharacter>().ForMember(gdto => gdto.Movies, opt => opt
                    .MapFrom(src => src.Id));

            // Mapping DTOEditCharacter to Character
            CreateMap<DTOEditCharacter, Character>();

            // Mapping DTOCreateCharacter to Character
            CreateMap<DTOCreateCharacter, Character>();
        }
    }
}
