﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using TheMoviesDB.Model;

namespace TheMoviesDB
{
    public class MoviesDbContext : DbContext
    {
        //Set the entities = add the properties for the dataset
        public DbSet<Movie> Movies { get; set; } //Table structure
        public DbSet<Character> Character { get; set; }
        public DbSet<Franchise> Franchise { get; set; }

        //customize the connection string
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(GetConnectionString());

        }
        public static string GetConnectionString()
        {
            //ConnectionStringBuilder - created connection string buildiner.
            SqlConnectionStringBuilder connectStringBuilder = new SqlConnectionStringBuilder();
            connectStringBuilder.DataSource = "localhost\\SQLEXPRESS";
            connectStringBuilder.InitialCatalog = "MoviesDB";
            connectStringBuilder.IntegratedSecurity = true;
            return connectStringBuilder.ConnectionString;
        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //seed data
            modelBuilder.Entity<Movie>().HasData(new Movie() { Id = 1, MovieTitle = "Captain Marvel", Genre = "Action", ReleaseYear = 2019, Director = "Anna Boden", Picture = "https://s3.amazonaws.com/com.marvel.terrigen/prod/captainmarvel_lob_crd_06.jpg", Trailer = "https://www.imdb.com/video/vi4235180569/?playlistId=tt4154664&ref_=tt_pr_ov_vi" });
            modelBuilder.Entity<Movie>().HasData(new Movie() { Id = 2, MovieTitle = "Avengers Assemble", Genre = "Action", ReleaseYear = 2012, Director = "Joss Whedon", Picture = "https://ichef.bbci.co.uk/images/ic/640x360/p09c88sz.jpg", Trailer = "https://www.imdb.com/video/vi1891149081/?playlistId=tt0848228&ref_=tt_ov_vi" });
            modelBuilder.Entity<Movie>().HasData(new Movie() { Id = 3, MovieTitle = "Cruella", Genre = "Comedy", ReleaseYear = 2021, Director = "Craig Gillespie", Picture = "https://lumiere-a.akamaihd.net/v1/images/p_cruella_21672_ba40c762.jpeg", Trailer = "https://www.imdb.com/video/vi1608499225/?playlistId=tt3228774&ref_=tt_ov_vi" });
            modelBuilder.Entity<Movie>().HasData(new Movie() { Id = 4, MovieTitle = "Saving Mr. Banks", Genre = "Drama", ReleaseYear = 2013, Director = "John Lee Hancock", Picture = "https://m.media-amazon.com/images/M/MV5BMTc0MTQ3NzE4Nl5BMl5BanBnXkFtZTcwMzA4NDM5OQ@@._V1_.jpg", Trailer = "https://www.imdb.com/video/vi2341120281/?playlistId=tt2140373&ref_=tt_ov_vi" });
            modelBuilder.Entity<Movie>().HasData(new Movie() { Id = 5, MovieTitle = "Finch", Genre = "Drama", ReleaseYear = 2021, Director = "Miguel Sapochnik", Picture = "https://m.media-amazon.com/images/M/MV5BMmExZDc4NjEtZjY1ZS00OWU5LWExZGYtYTc4NDM1ZmRhMDZhXkEyXkFqcGdeQXVyMTEyMjM2NDc2._V1_.jpg", Trailer = "https://www.imdb.com/video/vi2345255705/?playlistId=tt3420504&ref_=tt_ov_vi" });


            modelBuilder.Entity<Character>().HasData(new Character() { Id = 1, FullName = "Tom Hanks", Alias = "Finch", Gender = "Male", Picture = "https://www.imdb.com/name/nm0000158/mediaviewer/rm3040001536/?ref_=nm_ov_ph" });
            modelBuilder.Entity<Character>().HasData(new Character() { Id = 2, FullName = "Emma Stone", Alias = "Cruela", Gender = "Female", Picture = "https://www.imdb.com/name/nm1297015/mediaviewer/rm3228157440/?ref_=nm_ov_ph" });
            modelBuilder.Entity<Character>().HasData(new Character() { Id = 3, FullName = "Chris Evans", Alias = "Captain", Gender = "Male", Picture = "https://www.imdb.com/name/nm0262635/mediaviewer/rm1966443008/?ref_=nm_ov_ph" });
            modelBuilder.Entity<Character>().HasData(new Character() { Id = 4, FullName = "Scarlett Johansson", Alias = "Natasha", Gender = "Female", Picture = "https://www.imdb.com/name/nm0424060/mediaviewer/rm1916122112/?ref_=nm_ov_ph" });
            modelBuilder.Entity<Character>().HasData(new Character() { Id = 5, FullName = "Gwyneth Paltrow", Alias = "Pepper", Gender = "Female", Picture = "https://www.imdb.com/name/nm0000569/mediaviewer/rm4286821888/?ref_=nm_ov_ph" });



            modelBuilder.Entity<Franchise>().HasData(new Franchise() { Id = 1, Name = "Marvel", Description = "Marvel Comics is an American media and entertainment company regarded as one of the “big two” publishers in the comics industry." });
            modelBuilder.Entity<Franchise>().HasData(new Franchise() { Id = 2, Name = "Disney", Description = "Disney is an entertainment and media company. It produces and acquires television programs, and live-action films, and animated motion pictures." });

            // Seed m2m movie-character. Need to define m2m and access linking table
            modelBuilder.Entity<Movie>()
                .HasMany(p => p.Characters)
                .WithMany(m => m.Movies)
                .UsingEntity<Dictionary<string, object>>(
                    "MovieCharacter",
                    r => r.HasOne<Character>().WithMany().HasForeignKey("CharacterId"),
                    l => l.HasOne<Movie>().WithMany().HasForeignKey("MovieId"),
                    je =>
                    {
                        je.HasKey("MovieId", "CharacterId");
                        je.HasData(
                            new { MovieId = 1, CharacterId = 1 },
                            new { MovieId = 1, CharacterId = 2 }
                        );
                    });

        }
    }
}
