﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MoviesDb3.Migrations
{
    public partial class MoviesDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Character",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FullName = table.Column<string>(type: "nvarchar(35)", maxLength: 35, nullable: false),
                    Alias = table.Column<string>(type: "nvarchar(15)", maxLength: 15, nullable: true),
                    Gender = table.Column<string>(type: "nvarchar(7)", maxLength: 7, nullable: true),
                    Picture = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Character", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Franchise",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Franchise", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Movies",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MovieTitle = table.Column<string>(type: "nvarchar(40)", maxLength: 40, nullable: false),
                    Genre = table.Column<string>(type: "nvarchar(15)", maxLength: 15, nullable: false),
                    ReleaseYear = table.Column<int>(type: "int", maxLength: 4, nullable: false),
                    Director = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false),
                    Picture = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Trailer = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FranchiseId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Movies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Movies_Franchise_FranchiseId",
                        column: x => x.FranchiseId,
                        principalTable: "Franchise",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "MovieCharacter",
                columns: table => new
                {
                    MovieId = table.Column<int>(type: "int", nullable: false),
                    CharacterId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MovieCharacter", x => new { x.MovieId, x.CharacterId });
                    table.ForeignKey(
                        name: "FK_MovieCharacter_Character_CharacterId",
                        column: x => x.CharacterId,
                        principalTable: "Character",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MovieCharacter_Movies_MovieId",
                        column: x => x.MovieId,
                        principalTable: "Movies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Character",
                columns: new[] { "Id", "Alias", "FullName", "Gender", "Picture" },
                values: new object[,]
                {
                    { 1, "Finch", "Tom Hanks", "Male", "https://www.imdb.com/name/nm0000158/mediaviewer/rm3040001536/?ref_=nm_ov_ph" },
                    { 2, "Cruela", "Emma Stone", "Female", "https://www.imdb.com/name/nm1297015/mediaviewer/rm3228157440/?ref_=nm_ov_ph" },
                    { 3, "Captain", "Chris Evans", "Male", "https://www.imdb.com/name/nm0262635/mediaviewer/rm1966443008/?ref_=nm_ov_ph" },
                    { 4, "Natasha", "Scarlett Johansson", "Female", "https://www.imdb.com/name/nm0424060/mediaviewer/rm1916122112/?ref_=nm_ov_ph" },
                    { 5, "Pepper", "Gwyneth Paltrow", "Female", "https://www.imdb.com/name/nm0000569/mediaviewer/rm4286821888/?ref_=nm_ov_ph" }
                });

            migrationBuilder.InsertData(
                table: "Franchise",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[,]
                {
                    { 1, "Marvel Comics is an American media and entertainment company regarded as one of the “big two” publishers in the comics industry.", "Marvel" },
                    { 2, "Disney is an entertainment and media company. It produces and acquires television programs, and live-action films, and animated motion pictures.", "Disney" }
                });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "Id", "Director", "FranchiseId", "Genre", "MovieTitle", "Picture", "ReleaseYear", "Trailer" },
                values: new object[,]
                {
                    { 1, "Anna Boden", null, "Action", "Captain Marvel", "https://s3.amazonaws.com/com.marvel.terrigen/prod/captainmarvel_lob_crd_06.jpg", 2019, "https://www.imdb.com/video/vi4235180569/?playlistId=tt4154664&ref_=tt_pr_ov_vi" },
                    { 2, "Joss Whedon", null, "Action", "Avengers Assemble", "https://ichef.bbci.co.uk/images/ic/640x360/p09c88sz.jpg", 2012, "https://www.imdb.com/video/vi1891149081/?playlistId=tt0848228&ref_=tt_ov_vi" },
                    { 3, "Craig Gillespie", null, "Comedy", "Cruella", "https://lumiere-a.akamaihd.net/v1/images/p_cruella_21672_ba40c762.jpeg", 2021, "https://www.imdb.com/video/vi1608499225/?playlistId=tt3228774&ref_=tt_ov_vi" },
                    { 4, "John Lee Hancock", null, "Drama", "Saving Mr. Banks", "https://m.media-amazon.com/images/M/MV5BMTc0MTQ3NzE4Nl5BMl5BanBnXkFtZTcwMzA4NDM5OQ@@._V1_.jpg", 2013, "https://www.imdb.com/video/vi2341120281/?playlistId=tt2140373&ref_=tt_ov_vi" },
                    { 5, "Miguel Sapochnik", null, "Drama", "Finch", "https://m.media-amazon.com/images/M/MV5BMmExZDc4NjEtZjY1ZS00OWU5LWExZGYtYTc4NDM1ZmRhMDZhXkEyXkFqcGdeQXVyMTEyMjM2NDc2._V1_.jpg", 2021, "https://www.imdb.com/video/vi2345255705/?playlistId=tt3420504&ref_=tt_ov_vi" }
                });

            migrationBuilder.InsertData(
                table: "MovieCharacter",
                columns: new[] { "CharacterId", "MovieId" },
                values: new object[] { 1, 1 });

            migrationBuilder.InsertData(
                table: "MovieCharacter",
                columns: new[] { "CharacterId", "MovieId" },
                values: new object[] { 2, 1 });

            migrationBuilder.CreateIndex(
                name: "IX_MovieCharacter_CharacterId",
                table: "MovieCharacter",
                column: "CharacterId");

            migrationBuilder.CreateIndex(
                name: "IX_Movies_FranchiseId",
                table: "Movies",
                column: "FranchiseId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MovieCharacter");

            migrationBuilder.DropTable(
                name: "Character");

            migrationBuilder.DropTable(
                name: "Movies");

            migrationBuilder.DropTable(
                name: "Franchise");
        }
    }
}
