﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MoviesDb3.DataTObj.CTOMovie;
using MoviesDb3.DataTObj.DTOCharacter;
using TheMoviesDB;
using TheMoviesDB.Model;

namespace MoviesDb3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MoviesController : ControllerBase
    {
        private readonly MoviesDbContext _context;
        private readonly IMapper _mapper;

        public MoviesController(MoviesDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;

        }
        /// <summary>
        /// Get all movies from the MoviesDTO
        /// </summary>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        // GET: api/Movies
        [HttpGet] 
        public async Task<ActionResult<IEnumerable<Movie>>> GetMovies()
        {
            return await _context.Movies.ToListAsync();
        }

        /// <summary>
        /// Get movie by id
        /// </summary>
        // GET: api/Movies/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Movie>> GetMovie(int id)
        {
            var movie = await _context.Movies.FindAsync(id);

            if (movie == null)
            {
                return NotFound();
            }

            return movie;
        }

        /// <summary>
        /// Update movie by id
        /// </summary>
        // PUT: api/Movies/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovie(int id, Movie movie)
        {
            if (id != movie.Id)
            {
                return BadRequest();
            }

            _context.Entry(movie).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MovieExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Adds a new movie to the database
        /// </summary>
        /// <param name="movie"></param>
        /// <returns></returns>
        // POST: api/Movies
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Movie>> PostMovie(DTOCreateMovie movieDTO)
        {
            var MovieModel = _mapper.Map<Movie>(movieDTO);
            _context.Movies.Add(MovieModel);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMovie", new { id = MovieModel.Id }, movieDTO);
        }

        /// <summary>
        /// Delete movie 
        /// </summary>
        // DELETE: api/Movies/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMovie(int id)
        {
            var movie = await _context.Movies.FindAsync(id);
            if (movie == null)
            {
                return NotFound();
            }

            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool MovieExists(int id)
        {
            return _context.Movies.Any(e => e.Id == id);
        }
        /// <summary>
        /// Update characters in movies
        /// </summary>
        [HttpPut("{id}/Characters")]
        public async Task<IActionResult> UpdateCharacterMovie(int id, List<int> characters)
        {
            return await UpdateCharacterMovie(id, characters);
        }
        /// <summary>
        /// Get all characters in movies
        /// </summary>
        [HttpGet("AllCharactersInMovie")]
        public async Task<List<DTOReadCharacter>> GetAllCharactersInMovie(int movieId)
        {
            return await GetAllCharactersInMovie(movieId);
        }

    }
}
