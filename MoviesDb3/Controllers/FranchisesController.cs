﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MoviesDb3.DataTObj.DTOCharacter;
using MoviesDb3.DataTObj;
using TheMoviesDB;
using TheMoviesDB.Model;

namespace MoviesDb3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FranchisesController : ControllerBase
    {
        private readonly MoviesDbContext _context;

        public FranchisesController(MoviesDbContext context)
        {
            _context = context;
        }
        /// <summary>
        /// Get all franchices
        /// </summary>
        // GET: api/Franchises
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Franchise>>> GetFranchise()
        {
            return await _context.Franchise.ToListAsync();
        }
        /// <summary>
        /// Get specific franchice by ID
        /// </summary>
        // GET: api/Franchises/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Franchise>> GetFranchise(int id)
        {
            var franchise = await _context.Franchise.FindAsync(id);

            if (franchise == null)
            {
                return NotFound();
            }

            return franchise;
        }

        /// <summary>
        /// Update franchice by id
        /// </summary>
        // PUT: api/Franchises/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, Franchise franchise)
        {
            if (id != franchise.Id)
            {
                return BadRequest();
            }

            _context.Entry(franchise).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FranchiseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Add franchice to the database
        /// </summary>
        // POST: api/Franchise
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Franchise>> PostFranchise(Franchise franchise)
        {
            _context.Franchise.Add(franchise);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetFranchise", new { id = franchise.Id }, franchise);
        }

        /// <summary>
        /// Delete franchice
        /// </summary>
        // DELETE: api/Franchises/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            var franchise = await _context.Franchise.FindAsync(id);
            if (franchise == null)
            {
                return NotFound();
            }

            _context.Franchise.Remove(franchise);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Update movies in franchise
        /// </summary>
        [HttpPut("{id}/Movies")]
        public async Task<IActionResult> UpdateMovieFranchise(int id, List<int> movies)
        {
            return await UpdateMovieFranchise(id, movies);
        }
        /// <summary>
        /// Show all movies in franchice
        /// </summary>
        [HttpGet("AllMoviesInFranchise")]
        public async Task<ActionResult<List<DTOReadMovie>>> GetAllMoviesInFranchise(int franchiseId)
        {
            return await GetAllMoviesInFranchise(franchiseId);
        }
        /// <summary>
        /// Show all characters in franchise
        /// </summary>
        [HttpGet("AllCharactersInFranchise")]
        public async Task<ActionResult<List<DTOReadCharacter>>> GetAllCharactersInFranchise(int franchiseId)
        {
            return await GetAllCharactersInFranchise(franchiseId);
        }

        private bool FranchiseExists(int id)
        {
            return _context.Franchise.Any(e => e.Id == id);
        }
    }
}
