# Assingment 3 Movies Database

## Description
A database is created to store and manipulate movie characters. The application is been constructed in ASP.NET Core and comprise of a database made in SQL Server through EF Core
with a RESTful API to allow users to manipulate the data. The database stores information about characters, movies, and the franchises these movies belong to.

## Use
VS2022
.Net Core 6.00 or higher
Swagger
SQL Server Management Studio
A CI pipeline is been created to build this application as a Docker artifact.

## Install
NuGet Packages: Microsoft.EntityFrameworkCore.SQLServer, Microsoft.EntityFrameworkCore.Design, Microsoft.EntityFrameworkCore.Tools and AutoMapper.Extensions.Microsoft.DependencyInjection

## API Documentation as Endpoints:
Applies the same for Movies, Franchise and Character
- Retrieve a list of all movie characters.
- Retrieve a single movie character by its id.
- Create a new movie character.
- Update an existing movie character.
- Delete an existing movie character.

## Additional tools to help you get Started
* [How to Write a Good README File for Your GitHub Project](https://www.freecodecamp.org/news/how-to-write-a-good-readme-file/)

* [Similar project 1 ](https://gitlab.com/NicholasLennox/athlete-api-demo-full)

* [Similar project 2](https://gitlab.com/noroff-accelerate/dotnet/project/bookapi)

## Contributing
Meg Stefouli

## Licence
MIT
